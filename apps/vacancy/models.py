from datetime import datetime
from django.db import models

from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.models import ClusterableModel
from wagtail.snippets.models import register_snippet
from wagtail.snippets.edit_handlers import SnippetChooserPanel

from apps.utils.models import VacancyPage, City


@register_snippet
class Rubric(models.Model):
    title = models.CharField('Название рубрики', max_length=255)
    description = models.TextField('Описание рубрики', max_length=3000)

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('description'),
        ])
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Рубрика вакансий'
        verbose_name_plural = 'Вакансии: Рубрики'


@register_snippet
class Skill(models.Model):
    title = models.CharField('Навык', max_length=255)
    description = models.TextField('Описание навыка', max_length=3000)

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('description'),
        ])
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Навык'
        verbose_name_plural = 'Вакансии: Навыки'


@register_snippet
class ProjectGroup(ClusterableModel):
    title = models.CharField('Группа направлений', max_length=255)
    alt_title = models.CharField('Группа направлений', max_length=255)
    description = models.TextField('Описание группы направлений', max_length=3000)

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('alt_title'),
            FieldPanel('description'),
            InlinePanel('project', label="Направление компании"),
        ])
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Группа направлений компании'
        verbose_name_plural = 'Вакансии: Группы направлений компании'


class Project(models.Model):
    title = models.CharField('Распространенное название направления', max_length=255)
    alt_title = models.CharField('Альтернативное название направления', max_length=255)
    description = models.TextField('Описание направления', max_length=3000)
    project_group = ParentalKey(
        to=ProjectGroup,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Группа направлений',
        related_name='project',
    )

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('alt_title'),
            FieldPanel('description'),
            FieldPanel('project_group'),
        ])
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Направление компании'
        verbose_name_plural = 'Вакансии: Направления компании'


class Vacancy(Page):
    requirements = RichTextField('Требования', blank=True, max_length=5000)
    duties = RichTextField('Обязанности', blank=True, max_length=5000)
    conditions = RichTextField('Условия', blank=True, max_length=5000)

    is_wanted = models.BooleanField(default=False, verbose_name='Разыскивается')
    for_probies = models.BooleanField(default=False, verbose_name='Для стажеров')

    years_need = models.PositiveIntegerField("Мин. опыт, лет", null=True, blank=True)
    months_need = models.PositiveIntegerField("Мин. опыт, месяцев", null=True, blank=True)

    city = models.ForeignKey(
        to=City,
        verbose_name='Город',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    rubric = models.ForeignKey(
        to=Rubric,
        verbose_name='Рубрика',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    project = models.ForeignKey(
        to=Project,
        verbose_name='Направление',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    skills = ParentalManyToManyField(
        to=Skill,
        verbose_name='Требуемые навыки',
        blank=True,
    )

    created_at = models.DateTimeField("Дата создания вакансии", default=datetime.now)

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('requirements'),
            FieldPanel('duties'),
            FieldPanel('conditions'),
        ], heading='Контент вакансии', classname='collapsible'),
        MultiFieldPanel([
            FieldPanel('is_wanted'),
            FieldPanel('for_probies'),
            FieldPanel('years_need'),
            FieldPanel('months_need'),
            SnippetChooserPanel('city'),
            SnippetChooserPanel('rubric'),
            SnippetChooserPanel('project'),
            FieldPanel('skills'),
            FieldPanel('created_at'),
        ], heading='Параметры вакансии', classname='collapsible'),
    ]

    parent_page_types = [VacancyPage]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии'
