from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from wagtail.contrib.modeladmin.views import CreateView, EditView
from apps.vacancy.models import Vacancy


class VacancyModelAdmin(ModelAdmin):
    model = Vacancy
    menu_order = 10
    menu_icon = 'doc-full'
    menu_label = 'Вакансии'
    list_display = ['title', 'requirements']
    search_fields = ['title', 'requirements']
    ordering = ['-id']
    add_to_settings_menu = False
    exclude_from_explorer = True


modeladmin_register(VacancyModelAdmin)


