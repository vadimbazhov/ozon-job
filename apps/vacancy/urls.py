from django.conf.urls import url

from apps.vacancy.views import vacancies, vacancy


vacancy_patterns = [
    url(r'^$', vacancies, name='vacancies'),
    url(r'^(?P<pk>[0-9]+)/?$', vacancy, name='vacancy'),
]
