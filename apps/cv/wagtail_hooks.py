from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from apps.cv.models import ExternalCV, FriendCV, InternalCV


class InternalCVModelAdmin(ModelAdmin):
    model = InternalCV
    menu_order = 30
    menu_icon = 'form'
    menu_label = 'Внутренние резюме'
    list_display = ['about', 'superpower']
    search_fields = ['about', 'superpower', 'achievements']
    ordering = ['-id']
    add_to_settings_menu = False
    exclude_from_explorer = True


class ExternalCVModelAdmin(ModelAdmin):
    model = ExternalCV
    menu_order = 40
    menu_icon = 'form'
    menu_label = 'Внешние резюме'
    list_display = ['name', 'contact_info']
    search_fields = ['name', 'contact_info']
    ordering = ['-id']
    add_to_settings_menu = False
    exclude_from_explorer = True


class FriendCVModelAdmin(ModelAdmin):
    model = FriendCV
    menu_order = 50
    menu_icon = 'form'
    menu_label = 'Резюме друзей'
    list_display = ['name', 'email']
    search_fields = ['name', 'email']
    ordering = ['-id']
    add_to_settings_menu = False
    exclude_from_explorer = True


modeladmin_register(InternalCVModelAdmin)
modeladmin_register(FriendCVModelAdmin)
modeladmin_register(ExternalCVModelAdmin)
