import os
from datetime import datetime
from django.conf import settings
from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.documents.models import Document, AbstractDocument
from wagtail.documents.edit_handlers import DocumentChooserPanel

from apps.vacancy.models import Project
from apps.utils.models import City


class CustomDocument(AbstractDocument):
    @property
    def url(self):
        return os.path.join(settings.MEDIA_URL, f'documents/{self.filename}')

    admin_form_fields = Document.admin_form_fields


class CVBase(models.Model):
    is_dumped = models.BooleanField(default=False, verbose_name='Выгружено в CRM')
    created_at = models.DateTimeField("Дата создания", default=datetime.now)

    cv_file = models.ForeignKey(
        to=CustomDocument,
        null=True, blank=True,
        verbose_name='Резюме',
        on_delete=models.SET_NULL,
        related_name='+'
    )

    class Meta:
        abstract = True


class ExternalCV(CVBase):
    name = models.CharField('Имя', max_length=255)
    contact_info = models.CharField('Контактная информация', max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Резюме посетителя'
        verbose_name_plural = 'Резюме посетителей'


class FriendCV(CVBase):
    name = models.CharField('Имя', max_length=255)
    phone = models.CharField('Телефон', max_length=255, blank=True, null=True)
    email = models.CharField('Email', max_length=255, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Резюме друга'
        verbose_name_plural = 'Резюме друзей'


class InternalCV(CVBase):
    years_need = models.PositiveIntegerField("Опыт в Ozon, лет", null=True, blank=True)
    months_need = models.PositiveIntegerField("Опыт в Ozon, месяцев", null=True, blank=True)
    position = models.CharField('Текущая должность в Ozon', max_length=255, blank=True, null=True)
    boss = models.CharField('Текущая должность в Ozon', max_length=255, blank=True, null=True)
    about = models.TextField('О себе', max_length=1000, blank=True, null=True)
    superpower = models.TextField('«Суперсила»', max_length=1000, blank=True, null=True)
    achievements = models.TextField('Достижения', max_length=1000, blank=True, null=True)

    project = models.ForeignKey(
        to=Project,
        verbose_name='Отдел Ozon',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    city = models.ForeignKey(
        to=City,
        verbose_name='Город',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    def __str__(self):
        return f"{self.about[:10]}..."

    class Meta:
        verbose_name = 'Внутреннее резюме'
        verbose_name_plural = 'Внутренние резюме'
