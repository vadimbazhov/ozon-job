from django.conf.urls import url

from apps.cv.views import cv, cvs


cv_patterns = [
    url(r'^$', cvs, name='cvs'),
    url(r'^(?P<pk>[0-9]+)/?$', cv, name='cv'),
]
