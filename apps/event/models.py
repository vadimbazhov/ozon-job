from datetime import datetime
from django.db import models
from django.utils import timezone

from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.images.models import Image
from wagtail.images.edit_handlers import ImageChooserPanel
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.models import ClusterableModel
from wagtail.snippets.models import register_snippet
from wagtail.snippets.edit_handlers import SnippetChooserPanel

from apps.utils.models import EventPage, City


@register_snippet
class EventType(ClusterableModel):
    title = models.CharField('Тип события', max_length=255)
    description = models.TextField('Описание типа событий', max_length=3000, null=True, blank=True)

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('description'),
            InlinePanel('event_rubric', label="Рубрика"),
        ])
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тип мероприятия'
        verbose_name_plural = 'Мероприятия: Типы'


class EventRubric(models.Model):
    title = models.CharField('Название рубрики', max_length=255)
    description = models.TextField('Описание рубрики', max_length=3000, null=True, blank=True)
    event_type = ParentalKey(
        to=EventType,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Тип событий',
        related_name='event_rubric',
    )

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('description'),
        ])
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Рубрика мероприятия'
        verbose_name_plural = 'Мероприятия: Рубрики'


@register_snippet
class Person(models.Model):
    fio = models.CharField('ФИО', max_length=255)
    spec = models.CharField('Специальность', max_length=255, null=True, blank=True)
    profile = models.TextField('Профиль', max_length=1000, null=True, blank=True)
    photo = models.ForeignKey(
        to=Image,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Фото',
        related_name='+',
    )

    panels = [
        MultiFieldPanel([
            FieldPanel('fio'),
            FieldPanel('spec'),
            FieldPanel('profile'),
            FieldPanel('photo'),
        ])
    ]

    def __str__(self):
        return self.fio

    class Meta:
        verbose_name = 'Спикер/Ментор'
        verbose_name_plural = 'Мероприятия: Спикеры и Менторы'


class Event(Page):
    description = RichTextField('Описание', blank=True, max_length=10000)
    date_start = models.DateTimeField("Дата начала", default=datetime.now, blank=True, null=True)
    time_start = models.TimeField("Час начала", default=timezone.now, blank=True, null=True)
    date_end = models.DateTimeField("Дата окончания", default=datetime.now, blank=True, null=True)
    duration = models.CharField('Длительность (в своб. форме)', max_length=255, blank=True, null=True)
    promo = models.BooleanField(default=False, verbose_name='Показывать в промо-блоке')

    image = models.ForeignKey(
        to=Image,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Изображение',
        related_name='+',
    )

    city = models.ForeignKey(
        to=City,
        verbose_name='Город',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    type = models.ForeignKey(
        to=EventType,
        verbose_name='Тип',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    rubric = models.ForeignKey(
        to=EventRubric,
        verbose_name='Рубрика',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    persons = ParentalManyToManyField(
        to=Person,
        verbose_name='Менторы/Спикеры',
        blank=True,
    )

    created_at = models.DateTimeField("Дата создания", default=datetime.now)

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('description'),
            FieldPanel('duration'),
            ImageChooserPanel('image'),
            FieldPanel('date_start'),
            FieldPanel('time_start'),
            FieldPanel('date_end'),
        ], heading='Контент события', classname='collapsible'),
        MultiFieldPanel([
            FieldPanel('promo'),
            SnippetChooserPanel('city'),
            FieldPanel('type'),
            FieldPanel('rubric'),
            FieldPanel('persons'),
            FieldPanel('created_at'),
        ], heading='Настройки события', classname='collapsible'),
    ]

    parent_page_types = [EventPage]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Мероприятие'
        verbose_name_plural = 'Мероприятия'
