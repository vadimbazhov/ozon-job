from django.conf.urls import url

from apps.event.views import events, event, rubrics_by_type


event_patterns = [
    url(r'^$', events, name='events'),
    url(r'^(?P<pk>[0-9]+)/?$', event, name='event'),
    url(r'^rubrics_by_type/(?P<pk>\d+)/?$', rubrics_by_type, name='rubrics_by_type'),
]
