from django.http import JsonResponse

from apps.event.models import EventRubric, Event


def to_json(event):
    return {
        "id": event.id,
        "title": event.title,
        "date_start": event.date_start,
        "date_end": event.date_end,
        "time_start": event.time_start,
        "created_at": event.created_at,
        "description": event.description,
        "duration": event.duration,
        "promo": event.promo,
        "image": event.image.file.url,
        "event_type": event.type.id if event.type else None,
        "city": event.city.id if event.city else None,
        "rubric": event.rubric.id if event.rubric else None,
        "persons": [y.id for y in event.persons.all()],
    }


def events(request):
    evs = Event.objects.all()
    return JsonResponse([to_json(ev) for ev in evs if ev.live], safe=False)


def event(request, pk=None):
    return JsonResponse(to_json(Event.objects.get(pk=pk, live=True)), safe=False)


def rubrics_by_type(request, pk: str=None):
    rubrics: EventRubric = EventRubric.objects.filter(event_type=int(pk))

    return JsonResponse([{
        "id": el.id,
        "title": el.title,
    } for el in rubrics], safe=False)
