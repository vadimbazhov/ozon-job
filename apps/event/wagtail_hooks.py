from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from apps.event.models import Event


class EventsModelAdmin(ModelAdmin):
    model = Event
    menu_order = 20
    menu_icon = 'list-ul'
    menu_label = 'Мероприятия'
    list_display = ['title', 'description']
    search_fields = ['title', 'description']
    ordering = ['-id']
    add_to_settings_menu = False
    exclude_from_explorer = True


modeladmin_register(EventsModelAdmin)
