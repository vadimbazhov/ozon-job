import logging

from django.core.management.base import BaseCommand
from django.core.exceptions import ValidationError
from wagtail.core.models import Page, Group

from apps.utils.models import VacancyPage, EventPage

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Начальная настройка системы'

    def handle(self, *args, **options):
        self.init_sites()
        self.create_roles()

    def init_sites(self):
        logger.info('Удаление страниц изначально созданных Wagtail')
        Page.objects.filter(title='Welcome to your new Wagtail site!').delete()
        root_page = Page.objects.get(id=1)

        logger.info('Создание страницы для вакансий')
        try:
            root_page.add_child(instance=VacancyPage(
                title='Вакансии',
                slug='vacancies',
            ))
        except ValidationError:
            pass

        logger.info('Создание страницы для мероприятий')
        try:
            root_page.add_child(instance=EventPage(
                title='Мероприятия',
                slug='events',
            ))
        except ValidationError:
            pass

    def create_roles(self):
        """ Удаление встроенных групп """
        Group.objects.filter(name__in=['Editors', 'Moderators']).delete()
