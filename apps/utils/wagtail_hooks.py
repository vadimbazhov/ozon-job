from django.utils.html import format_html, format_html_join
from django.templatetags.static import static

from wagtail.core import hooks


@hooks.register('construct_main_menu')
def hide_menu_items(_, menu_items):
    menu_items[:] = [item for item in menu_items if item.name != 'explorer']


@hooks.register('construct_homepage_summary_items')
def clear_summary_items(request, items):
    del items[:]


@hooks.register('insert_editor_js')
def editor_js():
    js_files = [
        'js/events_admin_form.js',
    ]
    js_includes = format_html_join(
        '\n', '<script src="{0}"></script>',
        ((static(filename),) for filename in js_files)
    )

    return js_includes
