from django.db import models

from wagtail.images.models import Image
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.models import register_snippet


class VacancyPage(Page):
    parent_page_types = [Page]


class EventPage(Page):
    parent_page_types = [Page]


@register_snippet
class City(models.Model):
    title = models.CharField('Название города', max_length=255)
    is_important = models.BooleanField(default=False, verbose_name='Показывать в блоке городов на сайте')
    image = models.ForeignKey(
        to=Image,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Символ города',
        related_name='+',
    )

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('is_important'),
            ImageChooserPanel('image'),
        ])
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Город присутствия'
        verbose_name_plural = 'Города присутствия'


@register_snippet
class CoolStory(models.Model):
    title = models.CharField('Заголовок истории', max_length=255)
    fio = models.CharField('ФИО', max_length=255, null=True, blank=True)
    spec = models.CharField('Специальность', max_length=255, null=True, blank=True)
    description = RichTextField('Описание', null=True, blank=True, max_length=10000)
    show_on_tech = models.BooleanField(default=False, verbose_name='Показывать на Ozon.Tech')
    show_on_probations = models.BooleanField(default=False, verbose_name='Показывать в Стажировках')

    photo = models.ForeignKey(
        to=Image,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Фото',
        related_name='+',
    )

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('fio'),
            FieldPanel('spec'),
            FieldPanel('description'),
            ImageChooserPanel('photo'),
            FieldPanel('show_on_tech'),
            FieldPanel('show_on_probations'),
        ]),
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'История успеха'
        verbose_name_plural = 'Истории успеха'
