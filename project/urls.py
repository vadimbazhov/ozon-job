from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic import RedirectView

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from apps.vacancy.urls import vacancy_patterns
from apps.event.urls import event_patterns
from apps.cv.urls import cv_patterns
#from apps.search.urls import search_patterns


urlpatterns = [
    url(r'^pages/', include(wagtail_urls)),
    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^favicon\.ico$', RedirectView.as_view(url=staticfiles_storage.url('images/favicon.png'))),
]

urlpatterns += [
    url(r'^vacancy/', include((vacancy_patterns, 'vacancy'), namespace='vacancy')),
    url(r'^event/', include((event_patterns, 'event'), namespace='event')),
    url(r'^cv/', include((cv_patterns, 'cv'), namespace='cv')),
    #url(r'^search/', include((search_patterns, 'search'), namespace='search')),
    url(r'', include(wagtail_urls)),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
