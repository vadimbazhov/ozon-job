from .base import *

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

STATIC_ROOT = os.path.join(BASE_DIR, 'ozon_static')
STATIC_URL = '/ozon_static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'ozon_media')
MEDIA_URL = '/ozon_media/'

try:
    from .local import *
except ImportError:
    pass
