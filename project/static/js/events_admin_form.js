function fillRubrics(type_id, selected_option = 0) {
    // Прежде всего очистим селектбокс рубрик. Первый пустой элемент - дефолтный в админке вогтейла.
    $('#id_rubric').empty();
    $('#id_rubric').append('<option value="">---------</option>');

    // Выбрали пустой тип событий. Первый элемент в селектбоксе. Просто выйдем.
    if (type_id === '') return 0;

    // Если тип был выбран, запросим его рубрики и поместим в селектбокс рубрик.
    $.get(
        "/event/rubrics_by_type/" + type_id,
        function(response) {
            if ( $.isEmptyObject(response) != true ) {
                $.each(response, function( el_num, el ) {
                    if (el.id === selected_option) {
                        var selected = 'selected=""';
                    }
                    else {
                        var selected = '';
                    }
                    $('#id_rubric').append('<option value="'+ el.id + '" ' + selected + '>' + el.title + '</option>');
                });
            }
        }
    );
}

function monitorTypeChange() {
    $('#id_type').change(function(){
        console.log( $(this).val() );
        fillRubrics($(this).val());
    });
}

$(document).ready(function() {
    // Запустим прослушивание события выбора типа мероприятий чтобы подгрузить актуальные рубрики для него.
    monitorTypeChange();

    // А так же подгрузим актуальные рубрики при рефреше с сохранением выбраной ранее рубрики.
    var event_type = $('#id_type').val();
    var selected_option = Number($('#id_rubric option:selected').val());
    fillRubrics(event_type, selected_option);
});

