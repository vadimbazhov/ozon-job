environment = development
development_env_file = environment/development.env
staging_env_file = environment/staging.env
production_env_file = environment/production.env

build:
	@make -s check
	@ln -sf environment/$(environment).env .env
	docker-compose build --pull

up:
	@make -s check
	@ln -sf environment/$(environment).env .env
	@if [ "$(environment)" = "production" ] ; then\
		docker-compose -f docker-compose.prod.yml up --build;\
	@else\
		docker-compose up --build;\
	fi

check:
	@if [ "$(environment)" = "development" ] && [ ! -f $(development_env_file) ] ; then\
		echo "File $(development_env_file) doesn't exist";\
		exit 1;\
	fi

	@if [ "$(environment)" = "staging" ] && [ ! -f $(staging_env_file) ] ; then\
		echo "File $(staging_env_file) doesn't exist";\
		exit 1;\
	fi

	@if [ "$(environment)" = "production" ] && [ ! -f $(production_env_file) ] ; then\
		echo "File $(production_env_file) doesn't exist";\
		exit 1;\
	fi

from_scratch:
	@if [ "$(shell readlink .env)" != "$(development_env_file)" ]; then\
		echo "This is too dangerous! Do it while on development environment only!";\
		exit 1;\
	fi

	docker-compose down -v
	for i in $(shell ls apps/*/migrations/*.py | fgrep -v '__init__.py'); do rm -f "$$i"; done
	docker-compose build --pull --no-cache --force-rm
	docker-compose up
