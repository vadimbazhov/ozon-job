#!/bin/bash

set -e

echo "Starting up backend server of $PROJECT_NAME project in '$ENVIRONMENT' environment"
echo "Postgres host and port ${POSTGRES_HOST} ${POSTGRES_PORT}"

echo "Waiting for Postgres"
wait-for-it.sh \
    --host=${POSTGRES_HOST} \
    --port=${POSTGRES_PORT} \
    --timeout=15 \
    --strict \
    -- \
    echo "Postgres is up"

echo "Variable DJANGO_SETTINGS_MODULE is set to $DJANGO_SETTINGS_MODULE value"

install -d /app/{media,static}
gosu root chown wagtail:wagtail media static -R

if [ "${ENVIRONMENT}" = "development" ]; then
    echo "Generating migrations"
    python manage.py makemigrations
fi

echo "Applying migrations"
python manage.py migrate

echo  "Setup Django/Wagtail"
python manage.py setup

echo "Collect static files"
python manage.py collectstatic --noinput --clear

if [ ${ENVIRONMENT} = 'development' ] && [ -n ${ADMIN_SECRET} ]; then

    script="
from django.contrib.auth.models import User;

username = 'admin';
password = ${ADMIN_SECRET};
email = 'admin@email.com';

if User.objects.filter(username=username).count()==0:
    User.objects.create_superuser(username, email, password);
    print('Superuser created.');
else:
    print('Superuser already exists. Skip his creation.');
"
    printf "$script" | python manage.py shell
fi

echo "Starting $@"
exec $@
