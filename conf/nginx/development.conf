user nginx;
worker_processes auto;
pid /var/run/nginx.pid;
daemon off;

events {
    worker_connections 1024;
    multi_accept on;
    use epoll;
}

http {
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    server_tokens off;
    charset utf-8;
    charset_types text/xml text/plain text/css application/javascript application/rss+xml
        application/json application/x-www-form-urlencoded multipart/form-data;


    # server_names_hash_bucket_size 64;
    # server_name_in_redirect off;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    gzip on;
    gzip_disable "msie6";

    server {
        listen 80 default_server;

        error_log /var/log/nginx/backend-error.log;
        access_log /var/log/nginx/backend-access.log;

        location / {
            proxy_pass http://backend:8000;
            client_max_body_size 4m;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
    }
}
