#!/usr/bin/env sh

set -e

echo "Starting Nginx server"

echo "Waiting for Backend"
wait-for-it.sh \
    --host=backend \
    --port=8000 \
    --timeout=60 \
    --strict \
    -- echo "Backend is up"
exec $@
